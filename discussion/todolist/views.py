from django.shortcuts import render
from django.http import HttpResponse
from .models import ToDoItem
# 'from' keyword allows importing of necessary classes, methods and other items needd in our application. While 'import' keyword defines what we are importing from the package

# Create your views here.
def index(request):
	todoitem_list = ToDoItem.objects.all()
	context = {'todoitem_list': todoitem_list}
	return render(request, "todolist/index.html", context)

def todoitem(request, todoitem_id):
	response = "You are viewing the details of %s"
	return HttpResponse(response % todoitem_id)